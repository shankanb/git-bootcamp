﻿using System;
using TechTalk.SpecFlow;
using Oops.Services;
using Oops.Models;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Oops.Test
{
    [Binding]
    public class AttendanceSteps
    {
        private PersonService _personService = new PersonService();
        private Person _person = new Person();
        private string _date;

        [Given(@"I have a student with Id as ""(.*)""")]
        public void GivenIHaveAStudentWithIdAs(int id)
        {
            _person = _personService.GetPerson(id);
        }
        
        [When(@"When I mark the student as present for ""(.*)""")]
        public void WhenWhenIMarkTheStudentAsPresentFor(string date)
        {
            _date = date;
            _person.Attendance.Add(new Attendance() { Date = DateTime.Parse(date), Present = true });
        }
        
        [Then(@"the attendance for the student for the day should be ""(.*)""")]
        public void ThenTheAttendanceForTheStudentForTheDayShouldBe(string presence)
        {
            var person = from p in _person.Attendance
                         where p.Date == DateTime.Parse(_date)
                         select p;
            Assert.Equal(person.FirstOrDefault().Present,Boolean.Parse(presence));
        }

        [BeforeScenario("markStudent")]
        public void AddTestData()
        {
            _personService.AddPerson(new Student() { Id = 1251515, Attendance = new List<Attendance>(), Marks=new List<int>()}); ;
        }
    }
}
