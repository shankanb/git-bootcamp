﻿using System;
using Oops.Repositories;
using Oops.Models;
using Oops.Services;
using System.Collections.Generic;

namespace Oops
{
    class Program
    {
        private static PersonRepository _personRepository = new PersonRepository();
        static void Main(string[] args)
        {
            _personRepository.Add(new Student()
            {
                Name = "AAA",
                Id = 123,
                Attendance = new List<Attendance>()
                {
                    new Attendance()
                    {
                        Date = DateTime.Parse("01-10-2020"),
                        Present = true
                    }
                },
                Marks = new List<int>() { 100, 80, 70 }
            });

            _personRepository.Add(new Student()
            {
                Name = "BBB",
                Id = 124,
                Attendance = new List<Attendance>()
                {
                    new Attendance()
                    {
                        Date = DateTime.Parse("01-10-2020"),
                        Present = true
                    }
                },
                Marks = new List<int>() { 100, 80, 70 }
            });

            _personRepository.Add(new Student()
            {
                Name = "CCC",
                Id = 125,
                Attendance = new List<Attendance>()
                {
                    new Attendance()
                    {
                        Date = DateTime.Parse("01-10-2020"),
                        Present = true
                    }
                },
                Marks = new List<int>() { 100, 80, 70 }
            });


            _personRepository.Add(new Student()
            {
                Name = "DDD",
                Id = 126,
                Attendance = new List<Attendance>()
                {
                    new Attendance()
                    {
                        Date = DateTime.Parse("01-10-2020"),
                        Present = true
                    }
                },
                Marks = new List<int>() { 100, 80, 70 }
            });


            _personRepository.Add(new Student()
            {
                Name = "EEE",
                Id = 127,
                Attendance = new List<Attendance>()
                {
                    new Attendance()
                    {
                        Date = DateTime.Parse("01-10-2020"),
                        Present = true
                    }
                },
                Marks = new List<int>() { 100, 80, 70 }
            });
            Student student = (Student)_personRepository.GetPerson(1);
            if (student == null)
            {
                Console.WriteLine("Student not found");
            }
            else
            {

                Console.WriteLine("Details for student id 123");
                Console.Write("Attendance: ");
                foreach (var att in student.Attendance)
                    Console.WriteLine(att + ",");
                Console.Write("Marks: ");
                foreach (var mark in student.Marks)
                    Console.Write(mark + ",");
                Console.WriteLine("\n\n");
            }
        }
    }
}
