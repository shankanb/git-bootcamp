﻿using Oops.Models;
using System;
using System.Collections.Generic;
using System.Text;
using Oops.Repositories;

namespace Oops.Services
{
    public class PersonService : IPersonService
    {
        private PersonRepository _personRepository = new PersonRepository();

        public List<Attendance> GetAttendance(int id)
        {
            var person = _personRepository.GetPerson(id);
            return person.Attendance;
        }

         public void MarkAttendance(Person person, Attendance attendance)
        {
            person.Attendance.Add(attendance);
        }

        public void AddPerson(Person person)
        {
            _personRepository.Add(person);
        }

        public Person GetPerson(int id)
        {
            return _personRepository.GetPerson(id);
        }
    }
}
