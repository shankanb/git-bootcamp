﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Oops.Models
{
    public class Student:Person
    {
        public Student()
        {
            Marks = new List<int>();
        }
        public List<int> Marks { get; set; }
    }
}
