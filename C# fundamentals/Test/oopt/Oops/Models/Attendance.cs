﻿using System;

namespace Oops.Models
{
    public class Attendance
    {
        public DateTime Date { get; set; }

        public bool Present { get; set; }

        public override string ToString()
        {
            return Date.ToString() + "-->" + Present.ToString();
        }
    }
}
