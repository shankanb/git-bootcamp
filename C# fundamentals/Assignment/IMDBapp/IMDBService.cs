﻿using System;
using System.Collections.Generic;
using System.Text;
using IMDBapp.Repository;
using IMDBapp.Domain;
using Newtonsoft.Json;

namespace IMDBapp
{
    public class IMDBService
    {
        public IMDBService()
        {
            _movieRepository = new MovieRepository();
            _actorRepository = new ActorRepository();
            _producerRepository = new ProducerRepository();
        }
        private MovieRepository _movieRepository;
        private ActorRepository _actorRepository;
        private ProducerRepository _producerRepository;
        public List<Movie> GetMovies()
        {
            return _movieRepository.Get();
        }
        public List<Producer> GetProducers()
        {
            return _producerRepository.Get();
        }
        public List<Actor> GetActors()
        {
            return _actorRepository.Get();
        }
        public Actor GetActor(string name)
        {
            return _actorRepository.Get(name);
        }
        public Producer GetProducer(string name)
        {
            return _producerRepository.Get(name);
        }
        public void AddMovie(Movie movies)
        {
            if (_actorRepository.Count == 0)
            {
                throw new IMDBException("There are no actors added");
            }
            if (_producerRepository.Count==0)
            {
                throw new IMDBException("There are no producers added");
            }
            if (string.IsNullOrEmpty(movies.Title))
            {
                throw new IMDBException("Title can't be empty");
            }
            if (string.IsNullOrEmpty(movies.Plot))
            {
                throw new IMDBException("Plot can't be empty");
            }
            if (movies.YearOfRelease==0)
            {
                throw new IMDBException("YearOfRelease can't be empty");
            }
            if (movies.Actors.Count<1)
            {
                throw new IMDBException("Movie should have at least one actor");
            }
            if (string.IsNullOrEmpty(movies.Producer.Name))
            {
                throw new IMDBException("Producer can't be empty");
            }
            _movieRepository.Add(movies);
        }


        public void AddActor(Actor actor)
        {
            if (string.IsNullOrEmpty(actor.Name))
            {
                throw new IMDBException("Name can't be empty");
            }
            if (string.IsNullOrEmpty(actor.DateOfBirth))
            {
                throw new IMDBException("Date of Birth can't be empty");
            }
            _actorRepository.Add(actor);
        }

        public void AddProducer(Producer producer)
        {
            if (string.IsNullOrEmpty(producer.Name))
            {
                throw new IMDBException("Name can't be empty");
            }
            if (string.IsNullOrEmpty(producer.DateOfBirth))
            {
                throw new IMDBException("Date of Birth can't be empty");
            }
            _producerRepository.Add(producer);
        }

   
        public void DeleteMovie(string title)
        {
            var movie = _movieRepository.Get(title);
            _movieRepository.Delete(movie);
        }

        public void ClearMovies()
        {
            _movieRepository.Clear();
        }
        public void ClearActors()
        {
            _actorRepository.Clear();
        }
        public void ClearProducers()
        {
            _producerRepository.Clear();
        }
    }
}
