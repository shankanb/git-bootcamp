﻿using System;
using System.Runtime.Serialization;

namespace IMDBapp
{
    [Serializable]
    public class IMDBException : Exception
    {
        public IMDBException()
        {
        }

        public IMDBException(string message) : base(message)
        {
        }

        public IMDBException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected IMDBException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}