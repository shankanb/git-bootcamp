﻿using System;
using System.Resources;
using Microsoft.VisualBasic;
using IMDBapp.Domain;
using System.Collections.Generic;

namespace IMDBapp
{
    class Program
    { 
        private static IMDBService _iMDBService = new IMDBService();
        public static void Main()
        {
            while (true)
            {
                Console.WriteLine("\n1)Add movie\n2)Add Actor\n3)Add Producer\n4)List Movie\n5)Delete Movie\n6)Exit\nWhat would yoou like to do?");
                int choice = int.Parse(Console.ReadLine());
                try
                {
                    switch (choice)
                    {
                        case 1:
                            if (_iMDBService.GetActors().Count < 1 || _iMDBService.GetProducers().Count < 1)
                            {
                                Console.WriteLine("No Actors or producers. Please add at least one actor or producer");
                                break;
                            }
                            AddMovie();
                            break;
                        case 2:
                            AddActor();
                            break;
                        case 3:
                            AddProducer();
                            break;
                        case 4:
                            ListMovies();
                            break;
                        case 5:
                            DeleteMovie();
                            break;
                        case 6:
                            return;
                        default:
                            Console.WriteLine("Invalid");
                            break;
                    }
                }
                catch(IMDBException e)
                {
                    Console.WriteLine(e.Message);
                }
                
            }
        }
        public static void AddActor()
        {
            Console.WriteLine("\nEnter Actor name: ");
            string name = Console.ReadLine();
            Console.WriteLine("\nEnter Actor DOB: ");
            string dob = Console.ReadLine();
            _iMDBService.AddActor(new Actor() { Name = name, DateOfBirth = dob });
        }

        public static void AddProducer()
        {
            Console.WriteLine("\nEnter Producer name: ");
            string name = Console.ReadLine();
            Console.WriteLine("\nEnter Producer DOB: ");
            string dob = Console.ReadLine();
            _iMDBService.AddProducer(new Producer() { Name = name, DateOfBirth = dob });
        }

        public static void AddMovie()
        {           
            Movie movie = new Movie();
            Console.WriteLine("\nEnter Movie name: ");
            string name = Console.ReadLine();
            Console.WriteLine("\nEnter Movie year of release: ");
            string yor = Console.ReadLine();
            while (!Information.IsNumeric(yor))
            {
                Console.WriteLine("\nYear of release must be a number\nEnter Movie year of release: ");
                yor = Console.ReadLine();
            }
            Console.WriteLine("\nEnter movie plot");
            string plot = Console.ReadLine();
            int i = 0;
            foreach(var actor in _iMDBService.GetActors())
            {
                Console.Write(++i + ") " + actor.Name+" ");
            }
            Console.WriteLine("\nChoose actors(Space separated values): ");
            string actorNumber = Console.ReadLine();
            List<Actor> actorList = new List<Actor>();
            foreach(var number in actorNumber.Split(" "))
            {
                var index = int.Parse(number)-1;
                actorList.Add(_iMDBService.GetActors()[index]);
            }


            i = 0;
            foreach (var producer in _iMDBService.GetProducers())
            {
                Console.Write(++i + ") " + producer.Name);
            }
            Console.WriteLine("\nChoose producer: ");
            int producerNumber = int.Parse(Console.ReadLine())-1;
            movie.Producer = _iMDBService.GetProducers()[producerNumber];
            movie.Actors = actorList;
            movie.Title = name;
            movie.YearOfRelease = int.Parse(yor);
            movie.Plot = plot;
            _iMDBService.AddMovie(movie);
        }

        public static void ListMovies()
        {
            var movies=_iMDBService.GetMovies();
            if (movies.Count == 0)
            {
                Console.WriteLine("No Movies");
                return;
            }
            foreach(var movie in movies)
            {
                Console.WriteLine("----------------------------------------------");
                Console.WriteLine("Movie title: "+movie.Title);
                Console.WriteLine("Movie Year of release: " + movie.YearOfRelease);
                Console.WriteLine("Movie Plot: " + movie.Plot);
                Console.WriteLine("Actors: ");
                foreach(var actors in movie.Actors)
                {
                    Console.WriteLine(actors.Name);
                }
                Console.WriteLine("Producer: " + movie.Producer.Name);
                Console.WriteLine("----------------------------------------------");
            }
        }

        public static void DeleteMovie()
        {
            var movieList = _iMDBService.GetMovies();
            int i = 0;
            foreach(var movie in movieList)
            {
                Console.WriteLine(++i + ") " + movie.Title);
            }
            Console.WriteLine("Choose a movie to delete: ");
            int number = int.Parse(Console.ReadLine());
            _iMDBService.DeleteMovie(_iMDBService.GetMovies()[number - 1].Title);
        }
    }
}
