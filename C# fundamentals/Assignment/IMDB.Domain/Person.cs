﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IMDBapp.Domain
{
    public class Person
    {
        public string Name { get; set; }
        public string DateOfBirth { get; set; }
    }
}
