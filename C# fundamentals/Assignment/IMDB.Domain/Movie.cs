﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IMDBapp.Domain
{
    public class Movie
    {
        //public Movies()
        //{
        //    Actors = new List<Actor>();
        //}

        public string Title { get; set; }
        public int YearOfRelease { get; set; }
        public string Plot { get; set; }
        //public List<string> Actors { get; set; }
        private List<Actor> _actors = new List<Actor>();
#pragma warning disable CA2227 // Collection properties should be read only
        public List<Actor> Actors
#pragma warning restore CA2227 // Collection properties should be read only
        {
            set { _actors = value; }
            get { return _actors; }
        }
        public Producer Producer { get; set; }
    }
}
