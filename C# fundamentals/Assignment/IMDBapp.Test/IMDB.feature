﻿Feature: IMDB
	In order to keep track of movies along with actors and producers who where a part of it
	I need a system to do that for me
	And show me all the movies along with actors and producers who where a part of it

@listMovies
Scenario: List movie
	Given I have the list of movies
	When I fetch the list of movies
	Then the list should look like
	| Title   | YearOfRelease	| Plot                                                                | Actors            | Producer       |
	| Titanic | 1997            | A lot of people take the Ice Bucket Challenge. It doesn’t end well. | Leonardo DiCaprio | James Cameroon |

@addMovie
Scenario: Add movie
	Given I have a movie with title "Titanic"
	And year of release is "1997"
	And plot is "A lot of people take the Ice Bucket Challenge. It doesn’t end well."
	And actors are "Leonardo DiCaprio"
	And producer is "James Cameroon"
	When I add the movies to the list
	Then my IMDB should have the added movie
	| Title   | YearOfRelease	| Plot                                                                | Actors            | Producer       |
	| Titanic | 1997            | A lot of people take the Ice Bucket Challenge. It doesn’t end well. | Leonardo DiCaprio | James Cameroon |

	Scenario: Add actor
	Given I have an actor with name "Leonardo DiCaprio"
	And actor Date of birth is "11-11-1974"
	When I add the actor to the list
	Then I should have the added actor '[{"Name":"Leonardo DiCaprio","DateOfBirth":"11-11-1974"}]'

Scenario: Add Producer
	Given I have an producer with name "James Cameroon"
	And producer Date of birth is "16-08-1954"
	When I add the producer to the list
	Then I should have the added producer '[{"Name":"James Cameroon","DateOfBirth":"16-08-1954"}]'

@deleteMovie
Scenario: Delete Movie
	Given I have a movie with the name "Titanic"
	When I delete the movie from the list
	Then The list should not have the deleted movie '[{"Title":"Titanic","YearOfRelease":1997,"Plot":"A lot of people take the Ice Bucket Challenge. It doesn’t end well.","Actors":[{"Name":"Leonardo DiCaprio","DateOfBirth":"DDMMYYYY"}],"Producer":{"Name":"James Cameroon","DateOfBirth":"DDMMYYYY"}}]'