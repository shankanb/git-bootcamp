﻿using System;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;
using IMDBapp.Domain;
using IMDBapp;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace IMDBapp.Test
{
    [Binding]
    public class IMDBSteps
    {

        private Movie _movies = new Movie();
        private Actor _actor = new Actor();
        private Producer _producer = new Producer();
        private IMDBService _iMDBService = new IMDBService();
        private readonly ScenarioContext scenarioContext;
        private string MovieTitle;

        public IMDBSteps(ScenarioContext context)
        {
            scenarioContext = context;
        }

        [Given(@"I have the list of movies")]
        public static void GivenIHaveTheListOfMovies()
        {
        }

        [Given(@"I have a movie with title ""(.*)""")]
        public void GivenIHaveAMovieWithTitle(string title)
        {
            _movies.Title = title;
        }

        [Given(@"year of release is ""(.*)""")]
        public void GivenYearOfReleaseIs(int year)
        {
            _movies.YearOfRelease = year;
        }

        [Given(@"plot is ""(.*)""")]
        public void GivenPlotIs(string plot)
        {
            _movies.Plot = plot;
        }

        [Given(@"actors are ""(.*)""")]
        public void GivenActorsAre(string actors)
        {
            foreach(var actor in actors.Split(","))
            {
                var tempActor=_iMDBService.GetActor(actor);
                _movies.Actors.Add(tempActor);
            }
            
        }

        [Given(@"producer is ""(.*)""")]
        public void GivenProducerIs(string producer)
        {
            var tempProducer = _iMDBService.GetProducer(producer);
            _movies.Producer = tempProducer;
        }


        [When(@"I fetch the list of movies")]
        public void WhenIFetchTheListOfMovies()
        {
            var movieList = _iMDBService.GetMovies();
            scenarioContext.Add("movieList", movieList);
        }

        [When(@"I add the movies to the list")]
        public void WhenIAddTheMoviesToTheList()
        {
            _iMDBService.AddMovie(_movies);
        }

        [Then(@"the list should look like")]
        public void ThenTheListShouldLookLike(Table table)
        {
            var movies = (List<Movie>)scenarioContext["movieList"];
            //table.CompareToInstance(movies.FirstOrDefault());

            for (var i = 0; i < movies.Count; i++)
            {
                string expectedMovieName = table.Rows[i]["Title"];
                string expectedMoviePlot = table.Rows[i]["Plot"];
                int expectedMovieYear = int.Parse(table.Rows[i]["YearOfRelease"]);
                string expectedMovieActors = table.Rows[i]["Actors"];
                string explectedMovieProducer = table.Rows[i]["Producer"];

                List<string> actorNames = new List<string>();
                foreach (var actor in movies[i].Actors)
                {
                    actorNames.Add(actor.Name);
                }
                string actualActors = string.Join(",", actorNames);

                if (!(expectedMovieName.Equals(movies[i].Title, StringComparison.Ordinal)
                    && expectedMoviePlot.Equals(movies[i].Plot, StringComparison.Ordinal)
                    && expectedMovieYear == movies[i].YearOfRelease
                    && expectedMovieActors.Equals(actualActors, StringComparison.Ordinal)
                    && explectedMovieProducer.Equals(movies[i].Producer.Name, StringComparison.Ordinal)))
                {
                    throw new IMDBException("Actual and expected values do not match");
                }
            }


        }

        [Then(@"my IMDB should have the added movie")]
        public void ThenMyIMDBShouldHaveTheAddedMovie(Table table)
        {
            var movies = _iMDBService.GetMovies();
            //table.CompareToInstance(movies.FirstOrDefault());

            for (var i = 0; i < movies.Count; i++)
            {
                string expectedMovieName = table.Rows[i]["Title"];
                string expectedMoviePlot = table.Rows[i]["Plot"];
                int expectedMovieYear = int.Parse(table.Rows[i]["YearOfRelease"]);
                string expectedMovieActors = table.Rows[i]["Actors"];
                string explectedMovieProducer = table.Rows[i]["Producer"];

                
                List<string> actorNames=new List<string>();
                foreach(var actor in movies[i].Actors)
                {
                    actorNames.Add(actor.Name);
                }
                string actualActors = string.Join(",", actorNames);

                if (!(expectedMovieName.Equals(movies[i].Title, StringComparison.Ordinal)
                    && expectedMoviePlot.Equals(movies[i].Plot, StringComparison.Ordinal)
                    && expectedMovieYear == movies[i].YearOfRelease
                    && expectedMovieActors.Equals(actualActors, StringComparison.Ordinal)
                    && explectedMovieProducer.Equals(movies[i].Producer.Name, StringComparison.Ordinal)))
                {
                    throw new IMDBException("Actual and expected values do not match");
                }
            }
        }


        [BeforeScenario("listMovies", "deleteMovie")]
        public void AddTestData()
        {
            _iMDBService.ClearActors();
            _iMDBService.AddActor(new Actor() { Name = "Leonardo DiCaprio", DateOfBirth = "DDMMYYYY" });

            _iMDBService.ClearProducers();
            _iMDBService.AddProducer(new Producer() { Name = "James Cameroon", DateOfBirth = "DDMMYYYY" });

            _iMDBService.ClearMovies();
            _iMDBService.AddMovie(new Movie() {
                Title = "Titanic",
                YearOfRelease = 1997,
                Plot = "A lot of people take the Ice Bucket Challenge. It doesn’t end well.",
                Producer = new Producer() { Name = "James Cameroon", DateOfBirth = "DDMMYYYY" },
                Actors = new List<Actor>() {
                    new Actor() { Name = "Leonardo DiCaprio", DateOfBirth = "DDMMYYYY" }
            } });
        }

        [Given(@"I have an actor with name ""(.*)""")]
        public void GivenIHaveAnActorWithName(string name)
        {
            _actor.Name = name;
        }

        [Given(@"actor Date of birth is ""(.*)""")]
        public void GivenActorDateOfBirthIs(string dob)
        {
            _actor.DateOfBirth = dob;
        }

        [When(@"I add the actor to the list")]
        public void WhenIAddTheActorToTheList()
        {
            _iMDBService.AddActor(_actor);
        }

        [Then(@"I should have the added actor '(.*)'")]
        public void ThenIShouldHaveTheAddedActor(string expectedValue)
        {
            var actorList = _iMDBService.GetActors();
            var actorListJson = JsonConvert.SerializeObject(actorList);
            Xunit.Assert.Equal(expectedValue, actorListJson);
        }

        [Given(@"I have an producer with name ""(.*)""")]
        public void GivenIHaveAnProducerWithName(string name)
        {
            _producer.Name = name;
        }

        [Given(@"producer Date of birth is ""(.*)""")]
        public void GivenProducerDateOfBirthIs(string dob)
        {
            _producer.DateOfBirth = dob;
        }

        [When(@"I add the producer to the list")]
        public void WhenIAddTheProducerToTheList()
        {
            _iMDBService.AddProducer(_producer);
        }

        [Then(@"I should have the added producer '(.*)'")]
        public void ThenIShouldHaveTheAddedProducer(string expectedValue)
        {
            var producerList = _iMDBService.GetProducers();
            var producerListJson = JsonConvert.SerializeObject(producerList);
            Xunit.Assert.Equal(expectedValue, producerListJson);
        }

        [Given(@"I have a movie with the name ""(.*)""")]
        public void GivenIHaveAMovieWithTheName(string title)
        {
            MovieTitle = title;
        }

        [When(@"I delete the movie from the list")]
        public void WhenIDeleteTheMovieFromTheList()
        {
            _iMDBService.DeleteMovie(MovieTitle);
        }
        [Then(@"The list should not have the deleted movie '(.*)'")]
        public void ThenTheListShouldNotHaveTheDeletedMovie(string deletedMovie)
        {
            var movieList = _iMDBService.GetMovies();
            var movieListJson = JsonConvert.SerializeObject(movieList);
            foreach (var movie in movieList)
            {
                if (deletedMovie.Equals(JsonConvert.SerializeObject(movie), StringComparison.Ordinal)) {
                    throw new IMDBException("Movie has not been deleted");
                }
            }
        }

        [BeforeScenario("addMovie")]
        public void AddActorsAndProducers()
        {
            _iMDBService.ClearActors();
            _iMDBService.AddActor(new Actor() { Name = "Leonardo DiCaprio", DateOfBirth = "DDMMYYYY" });

            _iMDBService.ClearProducers();
            _iMDBService.AddProducer(new Producer() { Name = "James Cameroon", DateOfBirth = "DDMMYYYY" });
        }
    }
}
