﻿using System;
using System.Collections.Generic;
using System.Text;
using IMDBapp.Domain;
using System.Linq;
namespace IMDBapp.Repository
{
    public class ActorRepository
    {
        public ActorRepository()
        {
            _actors = new List<Actor>();
        }
        private List<Actor> _actors;

        public List<Actor> Get()
        {
            return _actors;
        }
        public int Count { get { return _actors.Count; } }

        public void Add(Actor newActor)
        {
            _actors.Add(newActor);
        }


        public Actor Get(string name)
        {
            var actorByName = from actor in _actors
                              where actor.Name==name
                                 select actor;
            return actorByName.FirstOrDefault();

        }

        public void Clear()
        {
            _actors.Clear();
        }
    }
}
