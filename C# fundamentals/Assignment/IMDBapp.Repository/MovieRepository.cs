﻿using System;
using System.Collections.Generic;
using IMDBapp.Domain;
using System.Linq;
namespace IMDBapp.Repository
{
    public class MovieRepository
    {

        public MovieRepository()
        {
            _movies = new List<Movie>();
        }
        private List<Movie> _movies;

        public List<Movie> Get() {
            return _movies;
        }
        
        public void Add(Movie newMovie)
        {
            _movies.Add(newMovie);
        }


        public Movie Get(string title)
        {
            var selectedMovie = from movie in _movies
                                where movie.Title == title
                                select movie;
            return selectedMovie.FirstOrDefault();
        }

        public void Delete(Movie movies)
        {
            _movies.Remove(movies);
        }

        public void Clear()
        {
            _movies.Clear();
        }

    }
}
