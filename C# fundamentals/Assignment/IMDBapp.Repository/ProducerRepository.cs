﻿using System;
using System.Collections.Generic;
using System.Text;
using IMDBapp.Domain;
using System.Linq;
namespace IMDBapp.Repository
{
    public class ProducerRepository
    {
        public ProducerRepository()
        {
            _producers = new List<Producer>();
        }
        private List<Producer> _producers;

        public List<Producer> Get()
        {
            return _producers;
        }

        public void Add(Producer newProducer)
        {
            _producers.Add(newProducer);
        }

        public int Count
        {
            get
            {
                return _producers.Count;
            }
        }

        public Producer Get(string name)
        {
            var producerByname = from producer in _producers
                                 where producer.Name == name
                                 select producer;
            return producerByname.FirstOrDefault(); ;

        }

        public void Clear()
        {
            _producers.Clear();
        }
    }
}
